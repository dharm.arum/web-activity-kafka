# Install Apache Kafka

Download Apache Kafka from https://www.apache.org/dyn/closer.cgi?path=/kafka/2.2.0/kafka_2.12-2.2.0.tgz and untar it
> tar -xzf kafka_2.12-2.2.0.tgz
> cd kafka_2.12-2.2.0
# Start the server
 - First start the zookeeper as kafka uses zookeeper internally. More about zookeeper here - https://zookeeper.apache.org/
> \kafka_2.12-2.2.0\bin\windows>zookeeper-server-start.bat ..\..\config\zookeeper.properties
[2019-05-27 10:10:51,874] INFO Reading configuration from: ..\..\config\zookeeper.properties (org.apache.zookeeper.server.quorum.QuorumPeerConfig)
[2019-05-27 10:10:51,894] INFO autopurge.snapRetainCount set to 3 (org.apache.zookeeper.server.DatadirCleanupManager)
[2019-05-27 10:10:51,902] INFO autopurge.purgeInterval set to 0 (org.apache.zookeeper.server.DatadirCleanupManager)
...

- Now start the Kafka server:
> \kafka_2.12-2.2.0\bin\windows>kafka-server-start.bat ..\..\config\server.properties
[2019-05-27 10:13:56,324] INFO Registered kafka:type=kafka.Log4jController MBean (kafka.utils.Log4jControllerRegistration$)
[2019-05-27 10:13:56,890] INFO starting (kafka.server.KafkaServer)
...
# Start Springboot application
- Now start your Springboot application either locally or as a jar. Verify the application is successfully started and running.
# Publish message to Kafka using REST API
- Once the Springboot application started successfully, you can publish message to kafka server using the below REST API (POST), either using a curl command or from postman client.
>http://<host>:<port>:/web-activity/publish?message=InsiderPROTECT
# Verify the published & consumed messages
- To view the topic web-activity use below command:
> \kafka_2.12-2.2.0\bin\windows>kafka-topics.bat --list --bootstrap-server localhost:9092
As records are stored with in a topic, the above command will list web-activity topic.
- To view the published message under the topic web-acvitiy use below command:
> kafka_2.12-2.2.0\bin\windows>kafka-console-consumer.bat  --bootstrap-server localhost:9092 --from-beginning --topic web-activity
# Verify real-time data push
Keep the kafka-console-consumer console open. Now if you post the message using REST API http://host:port:/web-activity/publish?message=InsiderPROTECT, you can see that the message is logged in the kafka-console-consumer console real-time.

If you want to see GUI in action, you can also see the application getting real time data displayed at http://host:port/index.html
