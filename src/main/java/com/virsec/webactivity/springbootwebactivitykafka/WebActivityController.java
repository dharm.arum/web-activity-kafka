package com.virsec.webactivity.springbootwebactivitykafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/web-activity")
public class WebActivityController {
    private final Producer producer;

    @Autowired
    WebActivityController(Producer pProducer) {
        this.producer = pProducer;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(value = "/publish")
    public void sendMessageToKafkaTopic(@RequestParam("message") String pMessage) {
        this.producer.sendMessage(pMessage);
    }
}
