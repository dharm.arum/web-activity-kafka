package com.virsec.webactivity.springbootwebactivitykafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootWebActivityKafkaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWebActivityKafkaApplication.class, args);
	}

}
