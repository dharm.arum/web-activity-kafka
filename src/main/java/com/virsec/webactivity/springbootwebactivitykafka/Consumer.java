package com.virsec.webactivity.springbootwebactivitykafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class Consumer {
    private final Logger logger = LoggerFactory.getLogger(Consumer.class);

    @Autowired
    SimpMessagingTemplate messagingTemplate;

    @KafkaListener(topics = "web-activity", groupId = "group_id")
    public void consume(String pMessage) throws IOException {
        logger.info(String.format("#### -> Consumed message -> %s", pMessage));
        messagingTemplate.convertAndSend("/topic/navigation", pMessage);
    }
}
