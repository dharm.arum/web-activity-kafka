$("document").ready(function() {
    var socket = new SockJS('/ws-web-activity'),
        navigationArray = [],
        stompClient = Stomp.over(socket);
    //
    function viewLastOctet(d) {
        var truncatedName = name = d.name.toString().substring(0, d.r / 5),
            name;
        if (truncatedName !== d.name.toString()) {
            name = truncatedName.substring(0, truncatedName.length - 2) + "..";
        }
        return `${name}(${d.count})`;
    };

    function buildBubbleGraph(container, pData) {
        var bubbleContainer = d3.select(container),
            width = container.clientWidth,
            diameter = 450,
            data,
            height = diameter;
        //
        bubbleContainer.select('svg').remove();
        //
        var svg = bubbleContainer.append("svg")
            .attr("width", width)
            .attr("height", height);
        //
        var center = {
            x: width / 2,
            y: height / 2
        };
        //
        data = {"children": pData};
        var dataMax = d3.max(pData, function (d) {
            return d.count;
        });
        var dataScale = d3.scale.linear().domain([0, dataMax]).range([10, 450]);
        // Use the pack layout to initialize radius:
        var bubbleLayout = d3.layout.pack()
            .size([diameter, diameter])
            .sort(function (a, b) {
                return b["value"] > a["value"];
            })
            .value(function (d) {
                return dataScale(d.count);
            });
        //
        var nodes = bubbleLayout.nodes(data).filter(function (d) {
            return !d.children;
        });
        var force = d3.layout.force()
            .nodes(nodes)
            .gravity(-0.01)
            .charge(function (d) {
                return -Math.pow(d.count, 2.0) / 8;
            })
            .size([width, height]);
        //
        var dataMax = d3.max(data, function (d) {
            return d.count;
        });
        //
        var nodes = svg.selectAll("circle")
            .data(force.nodes())
            .enter()
            .append("circle")
            .attr("r", function (d) {
                //me.addToolTip(d, $(this));
                return d.r;
            })
            .attr("fill", function (d) {
                let color='';
                if(d.count >= 11) {
                    color='#ef5350';
                } else if(d.count <= 10 && d.count > 2) {
                    color='#ffa726';
                } else {
                    color = '#607B8D'
                }
                return color;
            });
        var texts = svg.selectAll("text")
            .data(force.nodes())
            .enter()
            .append("text")
            .style("font-family", function (d) {
                return "Arial";
            })
            .style("font-size", function (d) {
                return "12px";
            })
            .style("fill", function (d) {
                return "#FFF"//me.fillTextColor(d);
            })
            .style("pointer-events", function (d) {
                return "none";
            })
            .text(function (d) {
                return viewLastOctet(d);
            })
        texts
            .style("display", function (d) {
                var display = "none";
                if (this.textContent.length > 2) {
                    display = "inline";
                }
                return display;
            });
        var charge = function (d) {
            return -Math.pow(d.r, 2.0) / 8;
        };
        //
        var move_towards_center = function (alpha) {
            return function (d) {
                d.x = d.x + (center.x - d.x) * (0.1 + 0.02) * alpha * 0.4625;
                return d.y = d.y + (center.y - d.y) * (0.1 + 0.02) * alpha;
            };
        };
        //
        force.gravity(-0.01).charge(charge).friction(0.9).on("tick", function (e) {
            nodes.each(move_towards_center(e.alpha)).attr("cx", function (d) {
                return d.x;
            }).attr("cy", function (d) {
                return d.y;
            });

            texts.attr("x", function (d) {
                var textLength = 0,
                    offSet;
                try {
                    textLength = this.getComputedTextLength();
                } catch (e) {
                    //textLength = me.getTextLength(d.name, "12px Arial");
                }
                offSet = (textLength / 2) - d.x;
                return d.x - d.x - offSet;
            }).attr("y", function (d) {
                return d.y + 5;
            })

        });
        //
        force.start();
        //
    }
    function drawGraph(pNavigation) {
        let groupByData = [],
            data =[];
        //push some mocked data
        if(navigationArray.length === 0) {
            navigationArray.push("Host Monitoring");
            for(let i=0; i< 100; i++){
                if(i >= 0 && i <= 10){
                    navigationArray.push("Installation");
                } else if (i >= 11 && i <= 25){
                    navigationArray.push("Host Security");
                } else if (i >= 26 && i <= 30) {
                    navigationArray.push("Splunk");
                } else if (i >= 31 && i <= 50) {
                    navigationArray.push("Monitor");
                } else if (i >= 51 && i <= 60) {
                    navigationArray.push("Notifications");
                } else if(i >= 61 && i <= 75){
                    navigationArray.push("Monitor");
                } else {
                    navigationArray.push("Navigation" + i);
                }
            }
            navigationArray.push("InsiderPROTECT");
            navigationArray.push("App Maps");
            navigationArray.push("Admin Panel");
            navigationArray.push("Dashboard");
            navigationArray.push("Dashboard");
            navigationArray.push("Dashboard");
            navigationArray.push("Dashboard");
            navigationArray.push("Dashboard");
            navigationArray.push("Dashboard");
            navigationArray.push("Dashboard");
            navigationArray.push("Dashboard");
            navigationArray.push("Dashboard");
        };
        //
        pNavigation && navigationArray.push(pNavigation.body);
        groupByData = _.groupBy(navigationArray, function(pMessage) {
            return pMessage.toLowerCase().trim();
        });
        //
        Object.keys(groupByData).forEach(function(item) {
            data.push({
                name: groupByData[item][0],
                count: groupByData[item].length
            })
        });
        //
        console.log("grouped data ----> " + data);
        buildBubbleGraph($(".main-container")[0], data);
    }
    stompClient.connect({}, function(frame) {
        drawGraph();
        stompClient.subscribe('/topic/navigation', function(pNavigation) {
            drawGraph(pNavigation);
        }, function(error) {
            console.log("Connection Error" + error);
        })
    })
});